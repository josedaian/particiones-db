<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use PDOException;

class ReadCsvCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:read';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command read ratings csv';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'csv:read';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $csvFileName = "ratings.csv";
        try {
            $csvFile = public_path('assets/' . $csvFileName);
            $data = $this->readCSV($csvFile,array('delimiter' => ','));
            $bar = $this->output->createProgressBar(count($data));

            $bar->start();
            foreach($data as $key =>  $rating){
                if(is_array($rating) && count($rating) == 4 && $key > 0){
                    \DB::insert('insert into ratings (user_id, movie_id, rating, timestamp) values (?, ?, ?, ?)', $rating);
                    $bar->advance();
                }
            }
            $bar->finish();
            $this->newLine();

        } catch (PDOException $exception) {
            $this->error(sprintf('Failed to import %s file, %s', $csvFileName, $exception->getMessage()));
        }
    }

    /**
     * @param mixed $csvFile 
     * @param mixed $array 
     * @return (array|false)[] 
     */
    public function readCSV($csvFile, $array)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle)) {
            $line_of_text[] = fgetcsv($file_handle, 0, $array['delimiter']);
        }
        fclose($file_handle);
        return $line_of_text;
    }
}