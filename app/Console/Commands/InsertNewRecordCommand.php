<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use PDOException;

class InsertNewRecordCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'table:new-record';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command insert new record on rating table';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'table:new-record';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $userId = $this->ask('Enter user_id');
            $movieId = $this->ask('Enter movie_id');
            $rating = $this->ask('Enter rating');
            $timestamp = time();

            \DB::insert('insert into ratings (user_id, movie_id, rating, timestamp) values (?, ?, ?, ?)', [$userId, $movieId, $rating, $timestamp]);

            $this->info('New record in the house!');
        } catch (PDOException $exception) {
            $this->error('Failed to reset table. Error:'.$exception->getMessage());
        }
    }
}