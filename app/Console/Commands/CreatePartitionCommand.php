<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use PDOException;

class CreatePartitionCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'partition:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command create partitions for table ratings';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'partition:create';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $cantidad = $this->ask('Cantidad de particiones a crear?');

            if(!is_numeric($cantidad)){
                return $this->error('La cantidad debe ser un entero');
            }

            $this->call('laravel-mysql-partition', [
                'action' => 'create',
                '--table' => 'ratings',
                '--column' => 'id',
                '--method' => 'hash',
                '--number' => intval($cantidad),
            ]);
            // create --table=ratings --column="id" --method=HASH --number='.$cantidad

            $this->info('Success!!');
        } catch (PDOException $exception) {
            $this->error('Oops! Error:'.$exception->getMessage());
        }
    }
}