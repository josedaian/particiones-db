<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use PDOException;

class ResetPartitionCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'partition:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command reset the table for default state';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'partition:reset';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $this->callSilently('migrate:rollback');
            $this->callSilently('migrate');
            $this->call('csv:read');

            $this->info('Table reset with success');
        } catch (PDOException $exception) {
            $this->error('Failed to reset table. Error:'.$exception->getMessage());
        }
    }
}