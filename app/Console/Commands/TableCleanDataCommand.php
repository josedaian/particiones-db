<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;
use PDOException;

class TableCleanDataCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'table:clean-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command delete all records from table';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'table:clean-data';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $this->callSilently('migrate:rollback');
            $this->callSilently('migrate');

            $this->info('Table has cleanned with success');
        } catch (PDOException $exception) {
            $this->error('Failed to clean data from table table. Error:'.$exception->getMessage());
        }
    }
}