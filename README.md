# Particiones de Tablas - Mysql

### Requisitos
- Servidor Apache
- Mysql 5.6 o superior
- PHP >= 7.3
  - BCMath PHP Extension
  - Ctype PHP Extension
  - Fileinfo PHP Extension
  - JSON PHP Extension
  - Mbstring PHP Extension
  - OpenSSL PHP Extension
  - PDO PHP Extension
  - Tokenizer PHP Extension
  - XML PHP Extension


### Pasos para instalar
1. Clonar el repositorio
```
git clone https://gitlab.com/josedaian/particiones-db.git
```

2. Moverse al directorio donde se clono el proyecto
```
cd particiones-db
```
3. Instalar dependencias
```
composer install
```
4. Generamos nuestro archivo que contendrá las configuraciones de la aplicación y base de datos
```
cp .env.example .env
```
5. Generamos la clave unica de la aplicacion
```
php artisan key:generate
```
6. Editamos el arhivo .env con las credenciales de nuestra base de datos
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=particiones 
DB_USERNAME=tu-user
DB_PASSWORD=secret-pass
```
**Importante!!** El siguiente comando a ejecutar es el que creará la base de datos. La base de datos se creará con el nombre seteado en el campo `DB_DATABASE` del archivo **.env**



<br>
<br>


### Manipulando la estructura de la base de datos
###### Crear la base de datos
```
php artisan db:create
```
###### Crear la tabla ratings
```
php artisan migrate
```
###### Cargar la tabla con el archivo ratins.csv (se encuentra en public/assets/ratings.csv)
```
php artisan csv:read
```
###### Borrar la tabla ratings
```
php artisan migrate:rollback
```

###### Crear particiones
```
php artisan laravel-mysql-partition create --table=ratings --column="id" --method=HASH --number=10
```
El parámetro **--number** definirá la cantidad de particiones a crear

###### Resetear tabla
```
php artisan partition:reset
```

###### Borrar todos los datos de la tabla de ratings
```
php artisan table:clean-data
```

###### Insertar nuevos datos en la tabla de ratings
```
php artisan table:new-record
```